import React from 'react';
import Banner from '../components/Banner';

const PaginaInicio = () => {
  return <div>
    <Banner imagen={'../../images/BannerI.jpg'} texto={'¡Cuidemos nuestro hogar!'} />
  </div>
}

export default PaginaInicio;