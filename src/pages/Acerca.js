import React from 'react';
import Banner from '../components/Banner';

const Acerca = () => {
  return <div>
    <Banner imagen={'../../images/BannerA.JPG'} texto={'¡Contáctanos!'} />
  </div>
}

export default Acerca;