import React from 'react';
import Banner from '../components/Banner';

const Cartografia = () => {
  return <div>
    <Banner imagen={'../../images/BannerC.jpeg'} texto={'¡Ubiquémonos!'} />
  </div>
}

export default Cartografia;