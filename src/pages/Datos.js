import React from 'react';
import Banner from '../components/Banner';

const Datos = () => {
  return <div>
    <Banner imagen={'../../images/BannerD.jpeg'} texto={'¡Conozcamos nuestro municipio!'} />
  </div>
}

export default Datos;