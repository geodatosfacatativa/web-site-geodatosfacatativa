import React from 'react';
import Banner from '../components/Banner';

const Hidrografia = () => {
  return <div>
    <Banner imagen={'../../images/BannerH.jpg'} texto={'¡Nada más importante que el agua!'} />
  </div>
}

export default Hidrografia;