import React from 'react';
import Imagen from './Imagen';

const BotonMenu = ({accion}) => {

  return <div className={'w3-button w3-right'} onClick={accion}>
    <Imagen src={'../../images/imagenMenu.png'} alt={'Menu'} width={'30px'} height={'30px'} />
  </div>
}

export default BotonMenu;