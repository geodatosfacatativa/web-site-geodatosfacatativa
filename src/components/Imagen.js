import React from 'react';

const Imagen = ({src, alt, width, height, position, z_index}) => {
  return <img loading={'lazy'} src={src} alt={alt} width={width} height={height} style={{position: position, zIndex: z_index , objectFit: 'cover'}} />
}

export default Imagen;