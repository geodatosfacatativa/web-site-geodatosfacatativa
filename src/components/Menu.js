import React, {useRef, useState} from 'react';
import {Link} from 'react-router-dom';
import styled from 'styled-components';
import BotonMenu from './BotonMenu';
import Imagen from './Imagen';

const Menu = () => {
  const referenciaMenu = useRef(null);
  const [titulo, cambiarTitulo] = useState('Geodatosfacatativa');

  const desplegar = (objeto) => {
    objeto.style.height = (objeto.style.height === '')?'350px':'';
  }

  const cambiarPagina = (nuevoNombre, objeto) => {
    desplegar(objeto);
    cambiarTitulo(nuevoNombre);
  }

  return <div className={'w3-top'}>
    <div className={'w3-bar w3-wide w3-padding w3-card w3-black'}>
      <Link to={'/Inicio'} className={'w3-button'}>
        <Imagen src={'../../images/icon.png'} alt={'Logo'} width={'30px'} height={'30px'}/>
        <span className={'w3-margin-left'}>{titulo}</span>
      </Link>
      <BotonMenu accion={() => desplegar(referenciaMenu.current)}/>
      <ListaMenu ref={referenciaMenu}>
        <BotonBarra to={'/Inicio'} pagina={'Inicio'}
          onClick={() => cambiarPagina('Geodatosfacatativa', referenciaMenu.current)}>
            Inicio
        </BotonBarra>
        <BotonBarra to={'/Hidrografia'} pagina={'Hidrografia'}
          onClick={() => cambiarPagina('Geodatosfacatativa - Hidrografía', referenciaMenu.current)}>
          Hidrografía
        </BotonBarra>
        <BotonBarra to={'/Cartografia'} pagina={'Cartografia'}
          onClick={() => cambiarPagina('Geodatosfacatativa - Cartografía', referenciaMenu.current)}>
          Cartografía
        </BotonBarra>
        <BotonBarra to={'/Datos'} pagina={'Datos'}
          onClick={() => cambiarPagina('Geodatosfacatativa - Datos ambientales', referenciaMenu.current)}>
          Datos ambientales
        </BotonBarra>
        <BotonBarra to={'/Acerca'} pagina={'Acerca'}
          onClick={() => cambiarPagina('Geodatosfacatativa - Acerca de' ,referenciaMenu.current)}>
          Acerca de
        </BotonBarra>
        <div className={'w3-col l12 m12 s12 w3-padding'}></div>
        <div className={'w3-col l12 m12 s12 w3-padding'}></div>
        <a href="http://colegiocolombiahoy.com" target="_blank" rel="noreferrer">
          <Icono src="../../images/Colegio.png" alt=""/>
        </a>
        <a href="https://periodicoquycafa.netlify.app" target="_blank" rel="noreferrer">
          <Icono src="../../images/quycafa.png" alt=""/>
        </a>
        <a href="https://www.youtube.com/channel/UCkRG_pFpxL7bzJgApvx3B6A" target="_blank" rel="noreferrer">
          <Icono src="../../images/youtube.ico" alt=""/>
        </a>
        <a href="https://www.instagram.com/colegiocolombiahoy/?hl=es-la" target="_blank" rel="noreferrer">
          <Icono src="../../images/instagram.ico" alt=""/>
        </a>
        <a href="https://www.facebook.com/groups/681462871924702/" target="_blank" rel="noreferrer">
          <Icono src="../../images/facebook.ico" alt=""/>
        </a>
      </ListaMenu>
    </div>
  </div>;
}

const ListaMenu = styled.div`
  position: absolute;
  display: block;
  top: 60px;
  left: 0;
  width: 100%;
  height: 0;
  background: rgba(0,0,0,0.85);
  padding: 0;
  margin: 0;
  list-style: none;
  text-align: center;
  overflow: hidden;
  transition: height 280ms linear;
`;

const BotonBarra = styled(Link)`
  width: 100vw;
  height: 50px;
  padding: 5px;
  display: flex;
  justify-content: center;
  align-items: center;
  text-decoration: none;

  :hover {
    background: white;
    color: black;
  }
`;

const Icono = styled.img`
  width: 35px;
  height: 35px;
  margin-left: 10px;
  border-radius: 50%;
`;

export default Menu;