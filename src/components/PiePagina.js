import React from 'react';
import styled from 'styled-components';
import Image from './Imagen';

const PiePagina = () => {
  return <div className={'w3-black w3-margin-top'}>
    <div className={'w3-container'}>
      <div className={'w3-col l4 w3-center w3-margin-top'}>
        <h2>¡Visítanos!</h2>
        <p>
          <LinkPiePagina href={'https://bit.ly/38Bmemz'} target={'_blank'} rel={'noreferrer'}>
            <Image src={'../../images/ubicacion.ico'} width={'35px'} height={'35px'} />
            <span className={'w3-margin-left'} >Km 7 Vía Facatativá - El Rosal</span>
          </LinkPiePagina>
        </p>
        <p>
          <Image src={'../../images/telefono.ico'} width={'35px'} height={'35px'} />
          <span className={'w3-margin-left'} >+57 3172677185</span>
        </p>
      </div>
      <div className={'w3-col l4 w3-center w3-margin-top'}>
        <h2>¡Contáctanos!</h2>
        <p>
          <Image src={'../../images/correo.ico'} width={'35px'} height={'35px'} />
          <span className={'w3-margin-left'} >campestrecolombiahoy@yahoo.com</span>
        </p>
        <p>
          <LinkPiePagina href={'http://colegiocolombiahoy.com/'} target={'_blank'} rel={'noreferrer'}>
            <Image src={'../../images/Colegio.png'} width={'35px'} height={'35px'} />
            <span className={'w3-margin-left'} >colegiocolombiahoy.com</span>
          </LinkPiePagina>
        </p>
        <p>
          <LinkPiePagina href={'https://periodicoquycafa.netlify.app'} target={'_blank'} rel={'noreferrer'}>
            <Image src={'../../images/quycafa.png'} width={'35px'} height={'35px'} />
            <span className={'w3-margin-left'} >Periódico Quyca Fa</span>
          </LinkPiePagina>
        </p>
      </div>
      <div className={'w3-col l4 w3-center w3-margin-top'}>
        <h2>¡Síguenos en nuestras redes sociales!</h2>
        <p>
          <LinkPiePagina href={'https://www.facebook.com/groups/681462871924702/'} target={'_blank'} rel={'noreferrer'}>
            <Image src={'../../images/facebook.ico'} width={'35px'} height={'35px'} />
            <span className={'w3-margin-left'} >Facebook</span>
          </LinkPiePagina>
        </p>
        <p>
          <LinkPiePagina href={'https://www.instagram.com/colegiocolombiahoy/?hl=es-la'} target={'_blank'} rel={'noreferrer'}>
            <Image src={'../../images/instagram.ico'} width={'35px'} height={'35px'} />
            <span className={'w3-margin-left'} >Instagram</span>
          </LinkPiePagina>
        </p>
        <p>
          <LinkPiePagina href={'https://www.youtube.com/channel/UCkRG_pFpxL7bzJgApvx3B6A'} target={'_blank'} rel={'noreferrer'}>
            <Image src={'../../images/youtube.ico'} width={'35px'} height={'35px'} />
            <span className={'w3-margin-left'} >Youtube</span>
          </LinkPiePagina>
        </p>
      </div>
      <p className="w3-col l12 m12 s12 w3-center w3-margin-top w3-padding">Diseñado por Juan David Guevara Arévalo |
        geodatosfacatativa@gmail.com</p>
    </div>
  </div>
}

const LinkPiePagina = styled.a`
  text-decoration: none;
`;

export default PiePagina;