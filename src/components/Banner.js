import React from 'react';
import Imagen from './Imagen';
import styled from 'styled-components';

const Banner = ({imagen, texto}) => {
  return <Contenedor>
    <Imagen src={imagen} width={'100%'} height={'100%'} position={'absolute'} z_index={0}/>
    <TextoPrincipal className={'w3-xlarge'}>{texto}</TextoPrincipal>
  </Contenedor>
}

const Contenedor = styled.div`
  width: 100vw;
  height: 100vh;
`;

const TextoPrincipal = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 0;
  width: 100%;
  height: 100%;
  position: absolute;
  color: white;
  background-color: #000000af;
`;

export default Banner;