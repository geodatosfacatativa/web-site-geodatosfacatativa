import React from 'react';
import {BrowserRouter, Redirect, Route} from 'react-router-dom';
import 'w3-css/w3.css';
import Menu from './components/Menu';
import PiePagina from './components/PiePagina';
import PaginaInicio from './pages/paginaInicio';
import Hidrografia from './pages/Hidrografia';
import Cartografia from './pages/Cartografia';
import Datos from './pages/Datos';
import Acerca from './pages/Acerca';

const App = () => {
  return <BrowserRouter>
    <Menu/>
    <Route path={'/Inicio'} component={PaginaInicio}/>
    <Route path={'/Hidrografia'} component={Hidrografia}/>
    <Route path={'/Cartografia'} component={Cartografia}/>
    <Route path={'/Datos'} component={Datos}/>
    <Route path={'/Acerca'} component={Acerca}/>
    <Route exact path={'/'}>
      <Redirect to={'/Inicio'}/>
    </Route>
    <PiePagina/>
  </BrowserRouter>
}

export default App;
